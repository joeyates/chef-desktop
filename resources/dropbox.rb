# Adapted from https://github.com/peplin/dropbox-cookbook
# Copyright 2011, Chris Peplin

resource_name :desktop_dropbox

property :name, String, default: "dropbox"

actions :install
default_action :install

action :install do
  execute "apt-get-update" do
    command "apt-get update"
    ignore_failure true
    action :nothing
  end

  apt_repository "dropbox" do
    uri "http://linux.dropbox.com/ubuntu"
    key "1C61A2656FB57B7E4DE0F4C1FC918B335044912E"
    keyserver "pgp.mit.edu"
    # distribution "stable"
    components ["main"]
    not_if { node["platform"] == "mac_os_x" }
    notifies    :run, resources(execute: "apt-get-update"), :immediately
  end

  package "dropbox"
end
