resource_name :desktop_telegram

property :name, String, default: "telegram"

actions :install
default_action :install

action :install do
  apt_repository "telegram" do
    uri "http://ppa.launchpad.net/atareao/telegram/ubuntu"
    keyserver "keyserver.ubuntu.com"
    key "A3D8A366869FE2DC5FFD79C36A9653F936FD5529"
    distribution "xenial"
    components ["main"]
  end

  package "telegram"
end
