resource_name :desktop_user_dir

property :name, String, required: true
property :value, String, required: true
property :user, String, required: true

actions :set
default_action :set

action_class do
  include Desktop::UserDir
end

action :set do
  current_value = user_dir_get(
    new_resource.name, user: new_resource.user
  )

  user_dir_set(
    new_resource.name,
    new_resource.value,
    user: new_resource.user
  ) do
    only_if { new_resource.value != current_value }
  end
end
