resource_name :desktop_signal

property :name, String, default: "signal"

actions :install
default_action :install

action :install do
  cached_signal_gpg_path = ::File.join(
    Chef::Config["file_cache_path"], "signal.asc"
  )

  remote_file cached_signal_gpg_path do
    source "https://updates.signal.org/desktop/apt/keys.asc"
    mode 0644
    checksum "2aca20b81a56ba0fbe24bdf58a45023e58a38392d885068afe596785ccd95491"
    notifies :run, "bash[install signal GPG key]", :immediately
  end

  bash "install signal GPG key" do
    code "apt-key add #{cached_signal_gpg_path}"
    action :nothing
  end

  apt_repository "signal" do
    uri "https://updates.signal.org/desktop/apt"
    distribution "xenial"
    components ["main"]
  end

  package "signal-desktop"
end
