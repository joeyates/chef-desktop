resource_name :desktop_google_chrome

property :name, String, default: "google_chrome"

actions :install
default_action :install

action :install do
  cached_gpg_path = ::File.join(
    Chef::Config["file_cache_path"], "google_chrome.linux_signing_key.asc"
  )

  remote_file cached_gpg_path do
    source "https://dl.google.com/linux/linux_signing_key.pub"
    mode 0644
    checksum node["desktop"]["google_chrome"]["linux_signing_key"]["checksum"]
    notifies :run, "bash[install Chrome GPG key]", :immediately
  end

  bash "install Chrome GPG key" do
    code "apt-key add #{cached_gpg_path}"
    action :nothing
  end

  apt_repository "google_chrome" do
    uri "http://dl.google.com/linux/chrome/deb"
    distribution "stable"
    components ["main"]
  end

  package "google-chrome-stable"
end
