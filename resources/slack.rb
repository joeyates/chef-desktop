resource_name :desktop_slack

property :name, String, default: "slack"

actions :install
default_action :install

action :install do
  slack_deb_filename = "slack-desktop-4.4.0-amd64.deb"
  slack_deb_url = "https://downloads.slack-edge.com/linux_releases/#{slack_deb_filename}"

  cached_slack_deb_path = ::File.join(
    Chef::Config["file_cache_path"], slack_deb_filename
  )

  remote_file cached_slack_deb_path do
    source slack_deb_url
    mode 0644
    checksum "22dc90c14a845b9d789952d2c97d76bd54a350062a6d8d9b51097b91fa735940"
    notifies :run, "bash[install slack]", :immediately
  end

  bash "install slack" do
    code "dpkg -i #{cached_slack_deb_path}"
    action :nothing
  end
end
