resource_name :desktop_packages

property :name, String, default: "packages"

actions :install
default_action :install

action :install do
  packages = node["desktop"]["packages"] || []

  packages.each do |p|
    package p
  end
end
