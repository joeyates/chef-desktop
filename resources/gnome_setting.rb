resource_name :desktop_gnome_setting

property :name, String
property :value, String
property :user, String

actions :set
default_action :set

action_class do
  include Desktop::GnomeSetting

  def whyrun_supported?
    true
  end
end

action :set do
  current_value = gnome_setting_get(
    new_resource.name, user: new_resource.user
  )

  gnome_setting_set(
    new_resource.name,
    new_resource.value,
    user: new_resource.user
  ) do
    only_if { new_resource.value != current_value }
  end
end
