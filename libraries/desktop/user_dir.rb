require "shellwords"

module Desktop; end

module Desktop::UserDir
  def user_dir_get(name, user:)
    `sudo -i -u #{user} xdg-user-dir #{name}`.chomp
  end

  def user_dir_set(name, value, user:)
    command = %Q(sudo -i -u #{user} xdg-user-dirs-update --set #{name} #{value.shellescape})
    `#{command}`
  end
end
