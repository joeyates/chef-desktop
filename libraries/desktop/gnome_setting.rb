require "shellwords"

module Desktop; end

module Desktop::GnomeSetting
  def gnome_setting_get(name, user:)
    `sudo -i -u #{user} dconf read #{name}`.chomp
  end

  def gnome_setting_set(name, value, user:)
    command = %Q(sudo -i -u #{user} DISPLAY=:0 dconf write #{name} #{value.shellescape})
    `#{command}`
  end
end
